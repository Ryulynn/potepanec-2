require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET /show" do
    let(:test_product) { create(:product) }

    it "returns http success" do
      get potepan_product_path(test_product.id)
      expect(response).to have_http_status "200"
    end

    it "意図したページへrenderできているか" do
      get potepan_product_path(test_product.id)
      expect(response).to render_template(:show)
    end
  end
end
