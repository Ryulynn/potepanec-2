require 'rails_helper'

RSpec.feature "products/show.html.erbに正しい情報の表示", type: :feature do
  given(:test_image_0) { create(:image) }
  given(:test_image_1) { create(:image) }
  given(:test_variant) { create(:variant, is_master: true, images: [test_image_0, test_image_1]) }
  given(:test_product) { create(:product, master: test_variant) }

  background do
    visit potepan_product_path(test_product.id)
  end

  scenario "商品タイトル部分のHomeをクリックしたときに/potepanにアクセスすること" do
    within("div.main-wrapper") do
      click_link 'Home'
    end
    expect(current_path).to eq potepan_path
  end

  feature "データに合わせた商品の情報が表示されること" do
    scenario "商品名が正しいこと" do
      expect(page).to have_selector '#product_name', text: test_product.name
    end

    scenario "値段が正しいこと" do
      expect(page).to have_selector '#product_price', text: test_product.display_price
    end

    scenario "商品説明が正しいこと" do
      expect(page).to have_selector '#product_description', text: test_product.description
    end

    feature "商品画像が正しいこと" do
      feature "大きい画像の表示" do
        scenario "image_large_0が正しく表示できていること" do
          expect(page).to have_selector "#image_large_0 > img[src*='/spree/products/#{test_product.images.first.id}/large/#{test_product.images.first.attachment_file_name}']"
        end

        scenario "image_large_1が正しく表示できていること" do
          expect(page).to have_selector "#image_large_1 > img[src*='/spree/products/#{test_product.images.second.id}/large/#{test_product.images.second.attachment_file_name}']"
        end
      end

      feature "小さい画像の表示" do
        scenario "image_small_0が正しく表示できていること" do
          expect(page).to have_selector "#image_small_0 > img[src*='/spree/products/#{test_product.images.first.id}/small/#{test_product.images.first.attachment_file_name}']"
        end

        scenario "image_small_1が正しく表示できていること" do
          expect(page).to have_selector "#image_small_1 > img[src*='/spree/products/#{test_product.images.second.id}/small/#{test_product.images.second.attachment_file_name}']"
        end
      end
    end
  end
end
